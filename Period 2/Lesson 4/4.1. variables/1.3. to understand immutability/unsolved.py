my_integer = 10
my_float = 102.65
my_complex = 12j
my_string_1 = 'birinci sətir'
my_string_2 = "ikinci sətir"
my_string_3 = '''üçüncü sətir'''
my_string_4 = """dördüncü sətir"""
my_tuple = (1, 2, 205, 16.436, "salam dünya!")

# dəyişənlərin adreslərini id() funksiyasının köməkliyi ilə çap edin

print("old address of my_integer variable was:", id(my_integer))
# my_integer dəyişənin dəyərini dəyişərək onun adresinin də dəyişdiyini yoxlayın

print("old type of my_integer variable was:", type(my_integer))
# dəyişənə fərqli tipdə dəyər mənimsətdikdə onun tipinin avtomatik dəyişdiyini yoxlayın