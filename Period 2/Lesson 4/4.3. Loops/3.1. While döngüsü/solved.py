# -*- coding: utf-8 -*-

# 1. While döngüsü vasitəsilə sətri tərsinə (reversed) yazın

myStr = "Salam Dünya"
i = len(myStr)-1
while i >= 0:
    print(myStr[i], end="")
    i -= 1
print("")

# 2. While döngüsü vasitəsilə şam ağacı çəkin
i = 0
limit = 20
while i < limit:
    print(" " * (limit - i - 1), "*" * (i * 2 + 1))
    i += 1

# 3. While döngüsünü sonsuz sayda döndürün. Lakin döngünün sayı 10-a çatdıqda döngünü break vasitəsilə məcburi dayandırın
i = 0
while(True):
    print("Addım:", i)
    i += 1
    if i > 10:
        break

# 4. while döngüsünün vasitəsilə 1-100 aralığında yalnızca cüt rəqəmləri çap edin
i = 0
while i < 100:
    i += 1
    if i%2 > 0:
        continue
    print(i)